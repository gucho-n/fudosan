from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os
from time import sleep
import logging
import math

prefectures = ["hokkaidou","touhoku","kantou","hokurikukoushinetsu","toukai","kinki","chugoku","chugoku","shikoku","kyusyuokinawa"]
# 1.場合によっては、chromeドライバーをループしてトップページに戻るたびにchromeドライバーをインストールし続けないとダメ。
# ⇒コンソールに表示されているか否か＊forの位置重要
for pref in prefectures:
    # WebDriverのオプションを設定
    chrome_options = webdriver.ChromeOptions()
    # ヘッドレスモードで実行するかどうかのフラグ
    headless_mode = False
    # 本番環境では、バックグラウンド用のため、
    if headless_mode:
        chrome_options.add_argument('--headless')  # ヘッドレスモード（ブラウザを表示せずに動作させる）
        chrome_options.add_argument('--no-sandbox')  # サンドボックスを無効にする


    # Chrome WebDriverのパスを指定（Dockerコンテナ内のパスを指定）
    driver_path = ChromeDriverManager().install()
# WebDriverを起動
# 'tranAreaMap'関数を呼び出す要素を見つけてクリックする
# prefectures = ["hokkaidou","touhoku","kantou","hokurikukoushinetsu","toukai","kinki","chugoku","chugoku","shikoku","kyusyuokinawa"]
# for pref in prefectures:
    service = webdriver.chrome.service.Service(driver_path)
    service.start()
    driver = webdriver.Chrome(service=service, options=chrome_options)
    # 暗黙的な定期時間を30秒に設定（デフォルトは0秒）
    driver.implicitly_wait(30)
    driver.get('https://www.bit.courts.go.jp/app/top/pt001/h01')
    # 常にどの画面がひょうじされるか意識する
    print("開始！",driver.current_url)
    # ダウンロード先ディレクトリのパスを指定
    download_directory = "C:/testDownload"
    # ダウンロード前のファイル数を取得
    initial_file_count = len(os.listdir(download_directory))
    # ダウンロード先の指定
    prefs = {
        "download.default_directory": download_directory,
        "download.prompt_for_download": False,
        "download.directory_upgrade": True,
        "safebrowsing.enabled": True
    }
    chrome_options.add_experimental_option("prefs", prefs)
    driver.get('https://www.bit.courts.go.jp/app/top/pt001/h01')
    element = driver.find_element(By.CSS_SELECTOR, f".bit__clickablemap_map_{pref}")
    element.click()
    element = driver.find_element(By.ID, "bloc-tab")
    element.click()
    element = driver.find_element(By.XPATH, "/html/body/form/main/div/div[3]/div/div/div/fieldset/div/table/tbody/tr[1]/td/div[2]/div/div/button[1]")
    element.click()
    element = driver.find_element(By.XPATH, "/html/body/form/main/div/div[3]/div/div/div/div/div/div[2]/button")
    element.click()    
    pagenum = driver.find_element(By.XPATH, '/html/body/main/div/form/div[2]/div[2]/div[1]/div/p/span[1]')
    pagenum = pagenum.text
    result = math.ceil(int(pagenum) / 10)
    def calcPosition(result):
        if result >= 4:
            pagePosition = 8
        if result >= 3:
            pagePosition = 7
        elif result >= 2:
            pagePosition = 6
        elif result >= 2:
            pagePosition = 5
        elif result >= 1:
            pagePosition = 4
        return pagePosition
    pagePosition = calcPosition(result)
    pageXpath = f"/html/body/main/div/form/div[2]/div[2]/div[12]/nav/div/div[{pagePosition}]/a/span[1]"
    for pageCount in range(result):
        count = 0
        # pythonの『for』はrange(2,12)のtoのすうじはよまｎ
        for link in range(2,12):
            logging.debug("debug")
            print("***number**",link)
            xpath= f"/html/body/main/div/form/div[2]/div[2]/div[{link}]/div[2]/div[2]/a/img"
            try:
                # 要素が表示されるまで待機
                element = WebDriverWait(driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, xpath))
                )
                # 要素がクリック可能になるまで待機
                element = WebDriverWait(driver, 10).until(
                    EC.element_to_be_clickable((By.XPATH, xpath))
                )
                # 要素をクリック
                element.click()
                import time
                time.sleep(5)
                # ウィンドウハンドルを管理
                window_handles = driver.window_handles

                # ターゲットのウィンドウハンドルを取得
                target_window_handle = None
                for handle in window_handles:
                    if handle != driver.current_window_handle:
                        target_window_handle = handle

                # ターゲットのウィンドウに切り替える
                driver.switch_to.window(target_window_handle)
                print("ウィンドウを切り替えました")
                print(driver.current_url)
                # 要素が表示されるまで待機
                element = WebDriverWait(driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, "/html/body/form/main/div/div[3]/div/div[1]"))
                )
                # 要素がクリック可能になるまで待機
                element = WebDriverWait(driver, 10).until(
                    EC.element_to_be_clickable((By.XPATH, "/html/body/form/main/div/div[3]/div/div[1]"))
                )
                # 要素をクリック
                element.click()
                # 一番左のウィンドウに戻る
                driver.switch_to.window(window_handles[0])
                print("一番左のウィンドウに戻る")

                time.sleep(10)

                print("ダウンロードが完了しました。")
                if link == 11:
                    # 要素が表示されるまで待機
                    print("ループの確認")
                    element = WebDriverWait(driver, 10).until(
                    EC.visibility_of_element_located((By.XPATH, pageXpath))
                    )
                    # 要素がクリック可能になるまで待機
                    element = WebDriverWait(driver, 10).until(
                        EC.element_to_be_clickable((By.XPATH, pageXpath))
                    )
                    # 要素をクリック
                    element.click()
                # WebDriverWait(driver, 10).until(
                # lambda x: len(os.listdir(download_directory)) > initial_file_count
                # )
                print("次いけそう")
        # ここで必要な処理を追加
            except Exception as e:
                import traceback
                traceback.print_exc()
                driver.quit()
                break
    #             print(f"Error: {e}")
    # ブラウザを終了
