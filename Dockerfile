# ベースイメージの指定
FROM python:3.9
ENV PYTHONIOENCODING utf-8
ENV TZ="Asia/Tokyo"
ENV LANG=C.UTF-8
ENV LANGUAGE=en_US:en_US

# 必要なパッケージのインストール
RUN apt-get update && apt-get install -y wget unzip

# Google Chromeのインストール
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list
RUN apt-get update && apt-get install -y google-chrome-stable

# # Chrome WebDriverのインストール
# RUN wget -O /tmp/chromedriver.zip https://chromedriver.storage.googleapis.com/172.217.175.112/chromedriver_linux64.zip
# RUN unzip /tmp/chromedriver.zip -d /usr/local/bin/
# RUN chmod +x /usr/local/bin/chromedriver

# 必要なパッケージのインストール
COPY requirements.txt /tmp/
RUN pip install --upgrade pip && pip install -r /tmp/requirements.txt

# Jupyter Labの設定
RUN jupyter lab --generate-config
# RUN echo "c.NotebookApp.token = ''" >> ~/.jupyter/jupyter_notebook_config.py

# 一般ユーザーを作成
RUN useradd -m guchon
USER guchon

# ディレクトリの作成
WORKDIR /fudosan

# ポートの公開
EXPOSE 8888

# Xサーバーの接続先を設定
ENV DISPLAY=:0

# Jupyter Labの起動
CMD ["jupyter", "lab", "--ip=0.0.0.0", "--port=8888", "--allow-root", "--no-browser"]
